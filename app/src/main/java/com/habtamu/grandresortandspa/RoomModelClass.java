package com.habtamu.grandresortandspa;

import android.media.Image;
import android.widget.ImageView;

public class RoomModelClass {
    private String roomNumber;
    private String roomType;
    private int price;
    private String imageUri;


    public RoomModelClass() {

    }

    public RoomModelClass(String roomNumber, String roomType, int price, String imageUri) {
        this.roomNumber = roomNumber;
        this.roomType = roomType;
        this.price = price;
        this.imageUri = imageUri;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }
}
