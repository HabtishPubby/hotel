package com.habtamu.grandresortandspa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RoomListActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RoomListAddapter roomListAddapter;
    List<RoomModelClass>roomList;
    private DatabaseReference reference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_list);
        recyclerView=findViewById(R.id.room_list_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        roomList=new ArrayList<>();
        reference=FirebaseDatabase.getInstance().getReference("RoomList");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot postaSnapshot: dataSnapshot.getChildren()){
                RoomModelClass rlist=postaSnapshot.getValue(RoomModelClass.class);
                roomList.add(rlist);}
                roomListAddapter=new RoomListAddapter(RoomListActivity.this,roomList);
                recyclerView.setAdapter(roomListAddapter);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
Toast.makeText(getApplicationContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }
}
