package com.habtamu.grandresortandspa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class SelectedFoodActivity extends AppCompatActivity {
    TextView fdescription;
    TextView  foodPrice;
    ImageView iMage;
    Button btn;
    EditText txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_food);
        fdescription=findViewById(R.id.txtSelectView);
        foodPrice=findViewById(R.id.food_txt_view);
        txt=findViewById(R.id.numOfFood);
        btn=findViewById(R.id.btnFoodorder);
        iMage=findViewById(R.id.selIview);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=getIntent();
                int numOfFoo=Integer.parseInt(txt.getText().toString());

                fdescription.setText("FOODNAME"+": "+intent.getStringExtra("FOODNAME"));
                String price=intent.getStringExtra("FOODPRICE");
                int pri=Integer.parseInt(price);
                int result=pri*numOfFoo;
                foodPrice.setText(result);
                Toast.makeText(getApplicationContext(),"You are success fuly order food",Toast.LENGTH_LONG).show();
            }
        });
    }
}