package com.habtamu.grandresortandspa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FileUploadActivity extends AppCompatActivity {
    Button btnRoom;
    DatabaseReference dbref;
    EditText roomtype,roomNum, rprice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_upload);
        btnRoom=findViewById(R.id.btnRoom);
        roomtype=findViewById(R.id.roomtype);
        roomNum=findViewById(R.id.room_num);
        rprice=findViewById(R.id.rprice);
        dbref= FirebaseDatabase.getInstance().getReference().child("rooms");
        btnRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rmType=roomtype.getText().toString();
                String roomNumber=roomNum.getText().toString();
                int roomPrice= Integer.parseInt(rprice.getText().toString());

                FileUploadModel rmc=new FileUploadModel(roomNumber,rmType,roomPrice);
                dbref.push().setValue(rmc).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(FileUploadActivity.this, "data entered", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }
}
