package com.habtamu.grandresortandspa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class HotelActivty extends AppCompatActivity {
    ImageView imageView;
    Button room,forder,location,detailInfo;
    TextView textDescription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_activty);
        imageView=(ImageView) findViewById(R.id.grandImage);
        room=(Button) findViewById(R.id.book_room);
        forder=(Button) findViewById(R.id.order_food);
        location=(Button) findViewById(R.id.g_location);
        detailInfo=(Button) findViewById(R.id.more_detail);
        room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(HotelActivty.this,RoomListActivity.class);
                startActivity(intent);

            }
        });
        forder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fintent=new Intent(HotelActivty.this,FoodListActivity.class);
               startActivity(fintent);

            }
        });
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lintent=new Intent(HotelActivty.this,LocationActivity.class);
                startActivity(lintent);

            }
        });
        detailInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dintent =new Intent(HotelActivty.this,HotelDetailActivity.class);
                      startActivity(dintent);
            }
        });
    }
}