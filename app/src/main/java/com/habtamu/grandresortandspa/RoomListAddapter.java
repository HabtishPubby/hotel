package com.habtamu.grandresortandspa;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RoomListAddapter  extends RecyclerView.Adapter<RoomListAddapter.RoomViewHolder> {
    private Context rContext;
    private List<RoomModelClass> roomList;
    public RoomListAddapter(Context cntxt,List<RoomModelClass>list){
        this.rContext=cntxt;
        this.roomList=list;
    }
    @NonNull
    @Override
    public RoomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View roomView= LayoutInflater.from(rContext).inflate(R.layout.room_list_layout,parent,false);

        return new RoomViewHolder(roomView);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomViewHolder holder, int position) {
final RoomModelClass roomModelClass=roomList.get(position);
holder.roomNumber.setText(roomModelClass.getRoomNumber());
holder.roomPrice.setText(roomModelClass.getPrice());
holder.roomType.setText(roomModelClass.getRoomType());
holder.roomImage.setImageURI(Uri.parse(roomModelClass.getImageUri()));

holder.setItemClickListener(new ItemClickInterface() {
    @Override
    public void itemClicked(View v, int index) {
        Intent intent= new Intent(rContext,SelectedRoomActivity.class);
intent.putExtra("ROOMNUM",roomList.get(index).getRoomNumber());
intent.putExtra("ROOMTYPE",roomList.get(index).getRoomType());
intent.putExtra("ROOMPRICE",roomList.get(index).getPrice());
intent.putExtra("Image",roomList.get(index).getImageUri());
        rContext.startActivity(intent);

    }
});

    }

    @Override
    public int getItemCount() {
        return roomList.size();
    }

    public class RoomViewHolder extends RecyclerView.ViewHolder {
        private ImageView roomImage;
        private TextView roomType;
        private TextView roomNumber;
        private TextView roomPrice;
        ItemClickInterface itemClickListener;
        public RoomViewHolder(@NonNull View itemView) {
            super(itemView);
           roomImage= itemView.findViewById(R.id.room_image);
           roomType=itemView.findViewById(R.id.room_type);
           roomPrice=itemView.findViewById(R.id.room_price);
           roomNumber=itemView.findViewById(R.id.room_number);
        }

        public  void setItemClickListener(ItemClickInterface itemClickListener){
            this.itemClickListener=itemClickListener;
        }
        public void onClick(View v)
        {
            this.itemClickListener.itemClicked(v,getAdapterPosition());
        }


    }
}
