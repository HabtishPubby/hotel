package com.habtamu.grandresortandspa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class HotelDetailActivity extends AppCompatActivity {
    ImageView secondImage;
    TextView hotelDescription;
    Button bookRoom,orderFood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_detail);
        secondImage=(ImageView)findViewById(R.id.deescribImage);
        hotelDescription=(TextView)findViewById(R.id.des_txtview);
        bookRoom=(Button)findViewById(R.id.optinaalbook);
        orderFood=findViewById(R.id.optionalOrder_food);
        bookRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(HotelDetailActivity.this,RoomListActivity.class);
                startActivity(intent);
            }
        });
        orderFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentflist=new Intent(HotelDetailActivity.this,FoodListActivity.class);
                startActivity(intentflist);
            }
        });

    }
}
