package com.habtamu.grandresortandspa;

public class ImageUploadModel {
     private String imageUri;
     private  String name;

     public  ImageUploadModel() {
     }

     public  ImageUploadModel(String imageUri, String name) {
          this.imageUri = imageUri;
          this.name = name;
     }

     public String getImageUri() {
          return imageUri;
     }

     public void setImageUri(String imageUri) {
          this.imageUri = imageUri;
     }

     public String getName() {
          return name;
     }

     public void setName(String name) {
          this.name = name;
     }
}

