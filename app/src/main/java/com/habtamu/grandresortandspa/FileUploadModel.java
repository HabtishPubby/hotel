package com.habtamu.grandresortandspa;

public class FileUploadModel {
    String roomNum;
    String roomTyp;
    double rPrice;

    public FileUploadModel() {

    }

    public FileUploadModel(String roomNum, String roomTyp, double rPrice) {
        this.roomNum = roomNum;
        this.roomTyp = roomTyp;
        this.rPrice = rPrice;
    }

    public String getRoomNum() {
        return roomNum;
    }

    public void setRoomNum(String roomNum) {
        this.roomNum = roomNum;
    }

    public String getRoomTyp() {
        return roomTyp;
    }

    public void setRoomTyp(String roomTyp) {
        this.roomTyp = roomTyp;
    }

    public double getrPrice() {
        return rPrice;
    }

    public void setrPrice(double rPrice) {
        this.rPrice = rPrice;
    }
}
