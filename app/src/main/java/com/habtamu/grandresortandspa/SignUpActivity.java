package com.habtamu.grandresortandspa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity {
    TextView siuppage;
    FirebaseAuth mAuth;
    EditText sign_email,sign_passwor;
    Button signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        siuppage=findViewById(R.id.sign_up_des);
        sign_email=findViewById(R.id.sign_up_email);
        sign_passwor=findViewById(R.id.sign_up_password);
        signUp=findViewById(R.id.sign_up_button);
        mAuth=FirebaseAuth.getInstance();
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=sign_email.getText().toString();
                String pas=sign_passwor.getText().toString();
                mAuth.createUserWithEmailAndPassword(email,pas).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Intent intent= new Intent(SignUpActivity.this,HotelActivty.class);
                            startActivity(intent);
                        }else{
                            Toast.makeText(SignUpActivity.this, "can't create user please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });
    }
}
