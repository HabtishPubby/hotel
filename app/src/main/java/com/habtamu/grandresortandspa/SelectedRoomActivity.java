package com.habtamu.grandresortandspa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SelectedRoomActivity extends AppCompatActivity {
    Button btn;
    TextView textView;
    TextView price;
    ImageView rImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_room);
        textView=findViewById(R.id.roomtxt);
        rImage=findViewById(R.id.roomView);
        price=findViewById(R.id.price_of_room);
        btn=findViewById(R.id.roombtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=getIntent();
                textView.setText("ROOMTYPE"+": "+intent.getStringExtra("ROOMTYPE"));
               price.setText("ROOMPRICE"+": "+intent.getStringExtra("ROOMPRICE"));
                Toast.makeText(getApplicationContext(),"Your Are Succesfuly Book  aroom",Toast.LENGTH_LONG).show();
            }
        });
    }
}
