package com.habtamu.grandresortandspa;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.xml.transform.Templates;

public class FoodListAddapter extends RecyclerView.Adapter<FoodListAddapter.FoodViewHolder> {
    private Context context;
    private List<FoodItemModel> foodList;
    public FoodListAddapter(Context cntxt,List<FoodItemModel> list){
        this.context=cntxt;
        this.foodList=list;
    }
    @NonNull
    @Override
    public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View foodView= LayoutInflater.from(context).inflate(R.layout.food_list_layout,parent,false);

        return new FoodViewHolder(foodView);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodViewHolder holder, int position) {
        FoodItemModel uploadImage=foodList.get(position);
        holder.txtFood.setText(uploadImage.getFoodName());
        holder.txtFoodPrice.setText(uploadImage.getPrice());
        holder.foodImage.setImageURI(Uri.parse(uploadImage.getImage()));
        holder.setItemClickListener(new ItemClickInterface() {
            @Override
            public void itemClicked(View v, int index) {
                Intent  intent =new Intent(context,SelectedFoodActivity.class);
                intent.putExtra("FOODNAME",foodList.get(index).getFoodName());
                intent.putExtra("FOODPRICE",foodList.get(index).getPrice());
                intent.putExtra("FOODIMAGE",foodList.get(index).getImage());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return foodList.size();
    }

    public class FoodViewHolder extends RecyclerView.ViewHolder{
        private TextView txtFood,txtFoodPrice;
        private  ImageView foodImage;
        ItemClickInterface itemInterfece;

        public FoodViewHolder(@NonNull View itemView) {
            super(itemView);
            txtFood=itemView.findViewById(R.id.food_name);
            txtFoodPrice=itemView.findViewById(R.id.food_price);
            foodImage=itemView.findViewById(R.id.food_image);
            ItemClickInterface itemClickInterface;
        }
        public  void setItemClickListener(ItemClickInterface itemClickListener){
            this.itemInterfece=itemClickListener;
        }
        public void onClick(View v)
        {
            this.itemInterfece.itemClicked(v,getAdapterPosition());
        }
    }
}
