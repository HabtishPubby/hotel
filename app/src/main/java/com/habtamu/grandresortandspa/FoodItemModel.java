package com.habtamu.grandresortandspa;

public class FoodItemModel {
    private String foodName;
    private int price;
    private String imageUri;

    public FoodItemModel() {

    }

    public FoodItemModel(String foodName, int price, String image) {
        this.foodName = foodName;
        this.price = price;
        this.imageUri = image;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImage() {
        return imageUri;
    }

    public void setImage(String image) {
        this.imageUri = image;
    }
}
