package com.habtamu.grandresortandspa;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

public class ImageUploadActivity extends AppCompatActivity {
    EditText ename;
    Button upload,choosefile;
    ProgressBar progressBar;
    ImageView view;
    TextView  showView;
    private Uri mImageUri;
    private StorageReference storagereference;
    DatabaseReference databaseReference;
    private StorageTask uploadtask;
    private static int PICK_IMAGE_REQUEST=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_upload);
        ename=(EditText)findViewById(R.id.nameEdit);
        upload=(Button) findViewById(R.id.choose);
        choosefile=(Button)findViewById(R.id.choose);
        showView=(TextView)findViewById(R.id.show_image);
        view=(ImageView)findViewById(R.id.diplay_image) ;

        progressBar=(ProgressBar)findViewById(R.id.pbar);
        storagereference= FirebaseStorage.getInstance().getReference("Hotel Images");
        databaseReference= FirebaseDatabase.getInstance().getReference("Hotel Images").child("Room Images").child("Food ItemImages");
        uploadFile();
        choosefile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openFileChooer();

            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(uploadtask!=null && uploadtask.isInProgress()){
                    Toast.makeText(getApplicationContext(),"upload in progress", Toast.LENGTH_LONG).show();
                }
                else{
                    uploadFile();
                Intent intent =new Intent(ImageUploadActivity.this,FileUploadActivity.class);
                 startActivity(intent);}
            }
        });
        showView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
    private void openFileChooer(){
        Intent intent=new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,PICK_IMAGE_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && requestCode == RESULT_OK &&
                data != null && data.getData() != null) {
            mImageUri=data.getData();
            //Picasso.with(this).load(mImageUri).into(view);

        }
    }
    private  String getFileExtension(Uri uir){
        ContentResolver contentResolver=getContentResolver();
        MimeTypeMap mim=MimeTypeMap.getSingleton();
        return mim.getExtensionFromMimeType(contentResolver.getType(uir));
    }
    private void uploadFile(){
        if(mImageUri!=null){
            StorageReference reference=storagereference.child(System.currentTimeMillis()+ "."+
                    getFileExtension(mImageUri));
            uploadtask=  reference.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Handler handler=new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(0);
                        }
                    }, 500);
                    Toast.makeText(ImageUploadActivity.this, "image is uploaded successfuly", Toast.LENGTH_SHORT).show();
                    ImageUploadModel uploadImage=new ImageUploadModel(ename.getText().toString().trim(),
                            taskSnapshot.getUploadSessionUri().toString());
                    String upload=databaseReference.getKey();
                    databaseReference.child(upload).setValue(uploadImage);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(ImageUploadActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();

                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress=(100.0 * taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                    progressBar.setProgress((int)progress);
                }
            });
        }
        else{
            Toast.makeText(getApplicationContext(),"image not selected",Toast.LENGTH_LONG).show();
        }

    }}
